import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Widget _myListTile() {
    return ListTile(
      contentPadding: EdgeInsets.all(20),
      tileColor: Colors.greenAccent,
      leading: Icon(Icons.accessibility),
      title: SelectableText(
        'It can be selected',
        toolbarOptions: ToolbarOptions(copy: true, selectAll: true),
      ),
      trailing: SizedBox(
        height: 50,
        width: 50,
        child: Container(
          child: Text('I\'m a sized box'),
          color: Colors.red,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Widget of the week #3'),
      ),
      body: SizedBox.expand(
        child: ListView.builder(
          itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.all(20),
            child: _myListTile(),
          );
        },
          itemCount: 10,
        ),
      ),
    );
  }
}
